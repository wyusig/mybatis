## mybatis源码学习
> version: 3.3

mybatis的类型处理主要解决两个问题，数据库表字段类型和对象类型的相互转换，mybatis的org.apache.ibatis.type主要就是做这个事。



### 1、抽象设计

在mybatis的官网中（http://mybatis.github.io/mybatis-3/configuration.html#typeHandlers）关于类型转换有如下的描述

> Whenever MyBatis sets a parameter on a PreparedStatement or retrieves a value from a ResultSet, a TypeHandler is used to retrieve the value in a means appropriate to the Java type.

当MyBatis为PreparedStatement 设置参数时或者从ResultSet中获取数据时，会根据Java类型使用TypeHandler 去获取相应的值。

官网中也列出了每一个TypeHandler用来处理对应的JDBC类型和JAVA类型。



#### 1.1、TypeHandler接口

```java
public interface TypeHandler<T> {

  // 设置参数
  void setParameter(PreparedStatement ps, int i, T parameter, JdbcType jdbcType) throws SQLException;

  // 根据列名称取得结果
  T getResult(ResultSet rs, String columnName) throws SQLException;

  // 按列下标取得结果
  T getResult(ResultSet rs, int columnIndex) throws SQLException;

  // 存储过程取得结果
  T getResult(CallableStatement cs, int columnIndex) throws SQLException;
}
```

TypeHandler定义ORM类型转换都抽象方法，一个给PreparedStatement设置参数，两个获取ResultSet的结果，1个过去存储过程执行结果



#### 1.2、BaseTypeHandler

BaseTypeHandler实现了TypeHandler抽象方法，并且继续抽象出4个方法，需要交给具体类型的子类实现

```java
public abstract class BaseTypeHandler<T> extends TypeReference<T> implements TypeHandler<T> {

  protected Configuration configuration;

  public void setConfiguration(Configuration c) {
    this.configuration = c;
  }

  @Override
  public void setParameter(PreparedStatement ps, int i, T parameter, JdbcType jdbcType) throws SQLException {
    //特殊情况，设置NULL
    if (parameter == null) {
        ...
        //设成NULL
        ps.setNull(i, jdbcType.TYPE_CODE);
        ...
    } else {
      //非NULL情况，怎么设还得交给不同的子类完成, setNonNullParameter是一个抽象方法
      setNonNullParameter(ps, i, parameter, jdbcType);
    }
  }

  @Override
  public T getResult(ResultSet rs, String columnName) throws SQLException {
    T result = getNullableResult(rs, columnName);
    //通过ResultSet.wasNull判断是否为NULL
    if (rs.wasNull()) {
      return null;
    } else {
      return result;
    }
  }

  @Override
  public T getResult(ResultSet rs, int columnIndex) throws SQLException {
    T result = getNullableResult(rs, columnIndex);
    if (rs.wasNull()) {
      return null;
    } else {
      return result;
    }
  }

  @Override
  public T getResult(CallableStatement cs, int columnIndex) throws SQLException {
    T result = getNullableResult(cs, columnIndex);
	//通过CallableStatement.wasNull判断是否为NULL
    if (cs.wasNull()) {
      return null;
    } else {
      return result;
    }
  }

  //非NULL情况，怎么设参数还得交给不同的子类完成
  public abstract void setNonNullParameter(PreparedStatement ps, int i, T parameter, JdbcType jdbcType) throws SQLException;

  //以下3个方法是取得可能为null的结果，具体交给子类完成
  public abstract T getNullableResult(ResultSet rs, String columnName) throws SQLException;

  public abstract T getNullableResult(ResultSet rs, int columnIndex) throws SQLException;

  public abstract T getNullableResult(CallableStatement cs, int columnIndex) throws SQLException;

}
```

主要是针对空值进行了公共处理



#### 1.3、DateTypeHandler

以DateTypeHandler为例, 其代码也非常简单，在给PreparedStatement时，把java.util.Date转成Timestamp

```java
public class DateTypeHandler extends BaseTypeHandler<Date> {

  @Override
  public void setNonNullParameter(PreparedStatement ps, int i, Date parameter, JdbcType jdbcType)
      throws SQLException {
    ps.setTimestamp(i, new Timestamp((parameter).getTime()));
  }

  @Override
  public Date getNullableResult(ResultSet rs, String columnName)
      throws SQLException {
    Timestamp sqlTimestamp = rs.getTimestamp(columnName);
    if (sqlTimestamp != null) {
      return new Date(sqlTimestamp.getTime());
    }
    return null;
  }

  @Override
  public Date getNullableResult(ResultSet rs, int columnIndex)
      throws SQLException {
    Timestamp sqlTimestamp = rs.getTimestamp(columnIndex);
    if (sqlTimestamp != null) {
      return new Date(sqlTimestamp.getTime());
    }
    return null;
  }

  @Override
  public Date getNullableResult(CallableStatement cs, int columnIndex)
      throws SQLException {
    Timestamp sqlTimestamp = cs.getTimestamp(columnIndex);
    if (sqlTimestamp != null) {
      return new Date(sqlTimestamp.getTime());
    }
    return null;
  }
}
```



### 2、TypeHandler注册

TypeHandler的注册是在TypeHandlerRegistry处理，首先看一下mybatis自带的TypeHandler注册

```java
public final class TypeHandlerRegistry {
    
    // JdbcType与TypeHandler关联关系map
    private final Map<JdbcType, TypeHandler<?>> JDBC_TYPE_HANDLER_MAP = new EnumMap<JdbcType, TypeHandler<?>>(JdbcType.class);
    // java类型、JdbcType与TypeHandler关联关系map
    private final Map<Type, Map<JdbcType, TypeHandler<?>>> TYPE_HANDLER_MAP = new HashMap<Type, Map<JdbcType, TypeHandler<?>>>();
    // 固定的未知类型处理器
    private final TypeHandler<Object> UNKNOWN_TYPE_HANDLER = new UnknownTypeHandler(this);
    // 全部TypeHandler
    private final Map<Class<?>, TypeHandler<?>> ALL_TYPE_HANDLERS_MAP = new HashMap<Class<?>, TypeHandler<?>>();
    
    public TypeHandlerRegistry() {
        // 构造函数里注册系统内置的类型处理器
        // java类型与TypeHandler关联，会去找该TypeHandler的MappedJdbcTypes注解的值，没有的话会是null
        register(Boolean.class, new BooleanTypeHandler());
        register(boolean.class, new BooleanTypeHandler());
        // JdbcType与TypeHandler关联
        register(JdbcType.BOOLEAN, new BooleanTypeHandler());
        register(JdbcType.BIT, new BooleanTypeHandler());
        // 指定java类型、JdbcType与TypeHandler关联
        register(String.class, JdbcType.CHAR, new StringTypeHandler());
        ...
    }

    // JdbcType与TypeHandler关联
    public void register(JdbcType jdbcType, TypeHandler<?> handler) {
        JDBC_TYPE_HANDLER_MAP.put(jdbcType, handler);
    }

    // java类型与TypeHandler关联
    public <T> void register(Class<T> javaType, TypeHandler<? extends T> typeHandler) {
        register((Type) javaType, typeHandler);
    }

    private <T> void register(Type javaType, TypeHandler<? extends T> typeHandler) {
        // 找该TypeHandler的MappedJdbcTypes注解的值，没有的话会是null
        MappedJdbcTypes mappedJdbcTypes = typeHandler.getClass().getAnnotation(MappedJdbcTypes.class);
        if (mappedJdbcTypes != null) {
            for (JdbcType handledJdbcType : mappedJdbcTypes.value()) {
                register(javaType, handledJdbcType, typeHandler);
            }
            if (mappedJdbcTypes.includeNullJdbcType()) {
                register(javaType, null, typeHandler);
            }
        } else {
            register(javaType, null, typeHandler);
        }
    }

    // 指定java类型、JdbcType与TypeHandler关联
    public <T> void register(Class<T> type, JdbcType jdbcType, TypeHandler<? extends T> handler) {
        register((Type) type, jdbcType, handler);
    }

    private void register(Type javaType, JdbcType jdbcType, TypeHandler<?> handler) {
        if (javaType != null) {
            Map<JdbcType, TypeHandler<?>> map = TYPE_HANDLER_MAP.get(javaType);
            if (map == null) {
                map = new HashMap<JdbcType, TypeHandler<?>>();
                TYPE_HANDLER_MAP.put(javaType, map);
            }
            map.put(jdbcType, handler);
        }
        ALL_TYPE_HANDLERS_MAP.put(handler.getClass(), handler);
    }
}
```



### 3、自定义TypeHandler

从上面的TypeHandler注册可以看到，注册主要是确定java类型、JdbcType与TypeHandler的关联关系

mybatis提供了两个注解：@MappedTypes和@MappedJdbcTypes，把它们标注在你自定义的TypeHandler上，然后在mybatis-config.xml上指定你的TypeHandler即可



下面举个例子：

首先编写自定义TypeHandler

```java
@MappedTypes(String.class)
@MappedJdbcTypes(value={JdbcType.CHAR,JdbcType.VARCHAR}, includeNullJdbcType=true)
public class StringTrimmingTypeHandler implements TypeHandler<String> {

  @Override
  public void setParameter(PreparedStatement ps, int i, String parameter,
      JdbcType jdbcType) throws SQLException {
    ps.setString(i, trim(parameter));
  }

  @Override
  public String getResult(ResultSet rs, String columnName) throws SQLException {
    return trim(rs.getString(columnName));
  }

  @Override
  public String getResult(ResultSet rs, int columnIndex) throws SQLException {
    return trim(rs.getString(columnIndex));
  }

  @Override
  public String getResult(CallableStatement cs, int columnIndex)
      throws SQLException {
    return trim(cs.getString(columnIndex));
  }

  private String trim(String s) {
    if (s == null) {
      return null;
    } else {
      return s.trim();
    }
  }
}
```

然后注册到mybatis配置文件，两种注册方式，二选一
```xml
<typeHandlers>
  <!--  单独注册某个TypeHandler  -->
  <typeHandler handler="org.apache.ibatis.submitted.typehandler.StringTrimmingTypeHandler"/>
  <!--  为整个包下面的TypeHandler注册  -->
  <package name="org.apache.ibatis.submitted.typehandler"/>
</typeHandlers>
```



### 4、mybatis注册自定义TypeHandler

我们来看一下mybatis是如何读取配置文件的TypeHandler并进行注册的

```java
public class XMLConfigBuilder extends BaseBuilder {
    private void typeHandlerElement(XNode parent) throws Exception {
        if (parent != null) {
            for (XNode child : parent.getChildren()) {
                //如果是package
                if ("package".equals(child.getName())) {
                    String typeHandlerPackage = child.getStringAttribute("name");
                    //（一）调用TypeHandlerRegistry.register，去包下找所有类
                    typeHandlerRegistry.register(typeHandlerPackage);
                } else {
                    //如果是typeHandler
                    String javaTypeName = child.getStringAttribute("javaType");
                    String jdbcTypeName = child.getStringAttribute("jdbcType");
                    String handlerTypeName = child.getStringAttribute("handler");
                    Class<?> javaTypeClass = resolveClass(javaTypeName);
                    JdbcType jdbcType = resolveJdbcType(jdbcTypeName);
                    Class<?> typeHandlerClass = resolveClass(handlerTypeName);
                    //（二）调用TypeHandlerRegistry.register(以下是3种不同的参数形式)
                    if (javaTypeClass != null) {
                        if (jdbcType == null) {
                            typeHandlerRegistry.register(javaTypeClass, typeHandlerClass);
                        } else {
                            typeHandlerRegistry.register(javaTypeClass, jdbcType, typeHandlerClass);
                        }
                    } else {
                        typeHandlerRegistry.register(typeHandlerClass);
                    }
                }
            }
        }
    }
}
```

接下来就是调用TypeHandlerRegistry的方法进行注册

```java
public final class TypeHandlerRegistry {
    
    // 扫描注册整个包的TypeHandler
    public void register(String packageName) {
        ResolverUtil<Class<?>> resolverUtil = new ResolverUtil<Class<?>>();
        resolverUtil.find(new ResolverUtil.IsA(TypeHandler.class), packageName);
        Set<Class<? extends Class<?>>> handlerSet = resolverUtil.getClasses();
        for (Class<?> type : handlerSet) {
            //Ignore inner classes and interfaces (including package-info.java) and abstract classes
            if (!type.isAnonymousClass() && !type.isInterface() && !Modifier.isAbstract(type.getModifiers())) {
                register(type);
            }
        }
    }

    public void register(Class<?> typeHandlerClass) {
        boolean mappedTypeFound = false;
        MappedTypes mappedTypes = typeHandlerClass.getAnnotation(MappedTypes.class);
        if (mappedTypes != null) {
            for (Class<?> javaTypeClass : mappedTypes.value()) {
                register(javaTypeClass, typeHandlerClass);
                mappedTypeFound = true;
            }
        }
        if (!mappedTypeFound) {
            register(getInstance(null, typeHandlerClass));
        }
    }
}
```



