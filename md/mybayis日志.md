## mybatis源码学习
> version: 3.3

因为mybatis适配了多种日志框架，包括slf4j、jcl、log4j2、log4j、jdk14log，以及为自定义日志实现留出拓展接口

可以在mybatis-config.xml进行配置，如果没有配置则使用jar自动发现机制

### 1、使用示例及解析

示例配置文件: mybatis-config.xml
```xml
<!DOCTYPE configuration
    PUBLIC "-//mybatis.org//DTD Config 3.0//EN"
    "http://mybatis.org/dtd/mybatis-3-config.dtd">

<configuration>

    <settings>
    	<setting name="logImpl" value="NO_LOGGING"/>
    </settings>

</configuration>
```

在setting.logImpl中进行日志配置，其中value可以在Configuration构造函数中找到线索，其实value也可以直接写类全限定名

org.apache.ibatis.session.Configuration.Configuration()
```java
public class Configuration {
    ...
    //类型别名注册机
    protected final TypeAliasRegistry typeAliasRegistry = new TypeAliasRegistry();
    
    public Configuration() {
        ...
        typeAliasRegistry.registerAlias("SLF4J", Slf4jImpl.class);
        typeAliasRegistry.registerAlias("COMMONS_LOGGING", JakartaCommonsLoggingImpl.class);
        typeAliasRegistry.registerAlias("LOG4J", Log4jImpl.class);
        typeAliasRegistry.registerAlias("LOG4J2", Log4j2Impl.class);
        typeAliasRegistry.registerAlias("JDK_LOGGING", Jdk14LoggingImpl.class);
        typeAliasRegistry.registerAlias("STDOUT_LOGGING", StdOutImpl.class);
        typeAliasRegistry.registerAlias("NO_LOGGING", NoLoggingImpl.class);
        ...
    }
    ...
}
```

其中可以写类全限定名是因为在解析config文件的setting.logImpl时

org.apache.ibatis.builder.xml.XMLConfigBuilder.settingsElement
```java
public class XMLConfigBuilder extends BaseBuilder {

    //解析配置
    private void parseConfiguration(XNode root) {
        //设置
        settingsElement(root.evalNode("settings"));
    }
    private void settingsElement(XNode context) throws Exception {
        //显式定义用什么log框架，不定义则用默认的自动发现jar包机制
        configuration.setLogImpl(resolveClass(props.getProperty("logImpl")));
    }
}
```

其最终会调用到org.apache.ibatis.type.TypeAliasRegistry.resolveAlias

```java
public class TypeAliasRegistry {
    public <T> Class<T> resolveAlias(String string) {
        try {
            if (string == null) {
                return null;
            }
            //先转成小写再解析
            String key = string.toLowerCase(Locale.ENGLISH);
            Class<T> value;
            //原理就很简单了，从HashMap里找对应的键值，找到则返回类型别名对应的Class, 这就跟Configuration构造函数put进去的值对应上
            if (TYPE_ALIASES.containsKey(key)) {
                value = (Class<T>) TYPE_ALIASES.get(key);
            } else {
                //找不到，再试着将String直接转成Class(这样怪不得我们也可以直接用java.lang.Integer的方式定义，也可以就int这么定义)
                value = (Class<T>) Resources.classForName(string);
            }
            return value;
        } catch (ClassNotFoundException e) {
            throw new TypeException("Could not resolve type alias '" + string + "'.  Cause: " + e, e);
        }
    }
}
```

前面说过，如果没有配置则使用jar自动发现机制，其找实现类顺序为：slf4j、jcl、log4j2、log4j、jdk14log, 其顺序代码可以看下面的mybatis Log抽象中的LogFactory

### 2、mybatis Log抽象

![mybatis Log抽象](./img/Log.png)

#### 2.1、Log

```java
package org.apache.ibatis.logging;

public interface Log {
    
  boolean isDebugEnabled();

  boolean isTraceEnabled();

  void error(String s, Throwable e);

  void error(String s);

  void debug(String s);

  void trace(String s);

  void warn(String s);
}
```

上面是mybatis Log的代码，如果你对比jcl的Log或者slf4j的Logger，可以发现，它算是一个精简版的Log。

其抽象方法都是我们日常会经常使用到的，包括只保留trace、debug、warn、error日志级别，还有判断是否开启trace、debug级别日志

#### 2.2、LogFactory

```java
public final class LogFactory {

  //给支持marker功能的logger使用(目前有slf4j, log4j2)
  public static final String MARKER = "MYBATIS";

  //具体究竟用哪个日志框架，那个框架所对应logger的构造函数
  private static Constructor<? extends Log> logConstructor;

  static {
    //这边乍一看以为开了几个并行的线程去决定使用哪个具体框架的logging，其实不然
    //slf4j
    tryImplementation(new Runnable() {
      @Override
      public void run() {
        useSlf4jLogging();
      }
    });
    //common logging
    tryImplementation(new Runnable() {
      @Override
      public void run() {
        useCommonsLogging();
      }
    });
    //log4j2
    tryImplementation(new Runnable() {
      @Override
      public void run() {
        useLog4J2Logging();
      }
    });
    //log4j
    tryImplementation(new Runnable() {
      @Override
      public void run() {
        useLog4JLogging();
      }
    });
    //jdk logging
    tryImplementation(new Runnable() {
      @Override
      public void run() {
        useJdkLogging();
      }
    });
    //没有日志
    tryImplementation(new Runnable() {
      @Override
      public void run() {
        useNoLogging();
      }
    });
  }

  //单例模式，不得自己new实例
  private LogFactory() {
    // disable construction
  }

  //根据传入的类来构建Log
  public static Log getLog(Class<?> aClass) {
    return getLog(aClass.getName());
  }

  //根据传入的类名来构建Log
  public static Log getLog(String logger) {
    try {
      //构造函数，参数必须是一个，为String型，指明logger的名称
      return logConstructor.newInstance(new Object[] { logger });
    } catch (Throwable t) {
      throw new LogException("Error creating logger for logger " + logger + ".  Cause: " + t, t);
    }
  }

  //提供一个扩展功能，如果以上log都不满意，可以使用自定义的log
  public static synchronized void useCustomLogging(Class<? extends Log> clazz) {
    setImplementation(clazz);
  }

  public static synchronized void useSlf4jLogging() {
    setImplementation(org.apache.ibatis.logging.slf4j.Slf4jImpl.class);
  }
  ...
  private static void tryImplementation(Runnable runnable) {
    if (logConstructor == null) {
      try {
      	//这里调用的不是start,而是run！根本就没用多线程嘛！
        runnable.run();
      } catch (Throwable t) {
        // ignore
      }
    }
  }

  private static void setImplementation(Class<? extends Log> implClass) {
    try {
      Constructor<? extends Log> candidate = implClass.getConstructor(new Class[] { String.class });
      Log log = candidate.newInstance(new Object[] { LogFactory.class.getName() });
      log.debug("Logging initialized using '" + implClass + "' adapter.");
      //设置logConstructor,一旦设上，表明找到相应的log的jar包了，那后面别的log就不找了。
      logConstructor = candidate;
    } catch (Throwable t) {
      throw new LogException("Error setting Log implementation.  Cause: " + t, t);
    }
  }
}
```

LogFactory其实就三个功能: 

1、自动寻找日志实现，优先级最高的是org.apache.ibatis.logging.slf4j.Slf4jImpl

2、可以设置日志实现，比如给解析配置了setting.logImpl的mybatis配置文件时使用

3、获取Log，底层会调用日志实现的Log构造函数

接下来我们来看一下各个日志实现(也就是各种日志框架适配类)

### 3、jcl

其实现在org.apache.ibatis.logging.common，jcl全名是 Jakarta Commons Logging，这是早期的日志抽象框架(现在是slf4j)

>其寻找日志实现逻辑为：
>
>1、寻找org.apache.commons.logging.Log配置，如果有，则使用配置的日志实现框架
>
>2、寻找有没有log4j实现，如果有，则使用org.apache.commons.logging.impl.Log4JLogger
>
>3、寻找有没有jdk1.4日志实现，如果有，则使用org.apache.commons.logging.impl.Jdk14Logger
>
>4、如果都没有找到，则使用自带的org.apache.commons.logging.impl.SimpleLog

其具体适配代码如下:

```java
package org.apache.ibatis.logging.commons;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

public class JakartaCommonsLoggingImpl implements org.apache.ibatis.logging.Log {

  private Log log;

  public JakartaCommonsLoggingImpl(String clazz) {
    log = LogFactory.getLog(clazz);
  }

  @Override
  public boolean isDebugEnabled() {
    return log.isDebugEnabled();
  }

  @Override
  public boolean isTraceEnabled() {
    return log.isTraceEnabled();
  }

  @Override
  public void error(String s, Throwable e) {
    log.error(s, e);
  }

  @Override
  public void error(String s) {
    log.error(s);
  }

  @Override
  public void debug(String s) {
    log.debug(s);
  }

  @Override
  public void trace(String s) {
    log.trace(s);
  }

  @Override
  public void warn(String s) {
    log.warn(s);
  }
}
```

因为适配大都类似，也比较简单，后续日志框架的适配代码我就不贴了，直接给链接

### 4、jdbc

org.apache.ibatis.logging.jdbc目录里面并不是某一种日志框架的适配，而是把数据库操作执行细节打印到日志

其原理是使用java动态代理，代理Connection、Statement、PreparedStatement、ResultSet, 在其方法执行时打印调试日志

以Connection为例:

```java
public final class ConnectionLogger extends BaseJdbcLogger implements InvocationHandler {

    private Connection connection;

    private ConnectionLogger(Connection conn, Log statementLog, int queryStack) {
        super(statementLog, queryStack);
        this.connection = conn;
    }

    /**
     * 在invoke方法里面，打印特定方法的日志，并使用connection对象执行被调用方法
     */
    @Override
    public Object invoke(Object proxy, Method method, Object[] params)
            throws Throwable {
        try {
            if (Object.class.equals(method.getDeclaringClass())) {
                return method.invoke(this, params);
            }
            if ("prepareStatement".equals(method.getName())) {
                if (isDebugEnabled()) {
                    debug(" Preparing: " + removeBreakingWhitespace((String) params[0]), true);
                }
                PreparedStatement stmt = (PreparedStatement) method.invoke(connection, params);
                stmt = PreparedStatementLogger.newInstance(stmt, statementLog, queryStack);
                return stmt;
            } else if ("prepareCall".equals(method.getName())) {
                if (isDebugEnabled()) {
                    debug(" Preparing: " + removeBreakingWhitespace((String) params[0]), true);
                }
                PreparedStatement stmt = (PreparedStatement) method.invoke(connection, params);
                stmt = PreparedStatementLogger.newInstance(stmt, statementLog, queryStack);
                return stmt;
            } else if ("createStatement".equals(method.getName())) {
                Statement stmt = (Statement) method.invoke(connection, params);
                stmt = StatementLogger.newInstance(stmt, statementLog, queryStack);
                return stmt;
            } else {
                return method.invoke(connection, params);
            }
        } catch (Throwable t) {
            throw ExceptionUtil.unwrapThrowable(t);
        }
    }

    /**
     * 创建Connection都会调用这个方法，生成Connection代理对象
     */
    public static Connection newInstance(Connection conn, Log statementLog, int queryStack) {
        InvocationHandler handler = new ConnectionLogger(conn, statementLog, queryStack);
        ClassLoader cl = Connection.class.getClassLoader();
        return (Connection) Proxy.newProxyInstance(cl, new Class[]{Connection.class}, handler);
    }
}
```



#### 5、jdk14

[jdk14log适配代码](https://gitee.com/wyusig/mybatis/blob/master/src/main/java/org/apache/ibatis/logging/jdk14/Jdk14LoggingImpl.java)

#### 6、log4j

[log4jlog适配代码](https://gitee.com/wyusig/mybatis/blob/master/src/main/java/org/apache/ibatis/logging/log4j/Log4jImpl.java)

#### 7、log4j2
略

#### 8、nologging

不打印任何日志，实现方法都是空

[nologging适配代码](https://gitee.com/wyusig/mybatis/blob/master/src/main/java/org/apache/ibatis/logging/nologging/NoLoggingImpl.java)

#### 8、slf4j
略

#### 9、stdout

使用System.out.println、System.err.println打印到控制台

[stdout适配代码](https://gitee.com/wyusig/mybatis/blob/master/src/main/java/org/apache/ibatis/logging/stdout/StdOutImpl.java)




