## mybatis源码学习
> version: 3.3

mybatis里面有大量实现xml的地方，因此mybatis对java的xml解析进行了封装，使其使用起来更加方便优雅



### 1、java xml解析例子

在了解mybatis的xml解析之前，我们先看一个java xml解析例子

```java
package org.apache.ibatis.parsing;

import org.junit.Test;
import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.xml.sax.InputSource;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathFactory;

import java.io.StringReader;

import static org.junit.Assert.assertEquals;

public class XPathTest {

    @Test
    public void test() throws Exception {
        String xml = "<xml><user>zxd</user></xml>";
        StringReader stringReader = new StringReader(xml);

        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        DocumentBuilder builder = factory.newDocumentBuilder();
        Document document = builder.parse(new InputSource(stringReader));

        XPathFactory xPathFactory = XPathFactory.newInstance();
        XPath xpath = xPathFactory.newXPath();

        String user1 = (String) xpath.evaluate("/xml/user", document, XPathConstants.STRING);
        assertEquals("zxd", user1);

        Node userNode = (Node) xpath.evaluate("xml", document, XPathConstants.NODE);
        String user2 = (String) xpath.evaluate("user", userNode, XPathConstants.STRING);
        assertEquals("zxd", user2);
    }
}
```

如上所示，这是一个简单xml解析例子，从构建Document，到构建XPath， 最后使用XPath和Document来读取元素值



### 2、XPathParser 

XPathParser是一个XPath解析器，其主要是封装了XPath的方法，使得使用起来更方便



#### 2.1、XPathParser属性

```java
public class XPathParser {

    // Xml Document
    private Document document;
    // 是否校验dtd文件
    private boolean validation;
    // 定义了EntityResolver就不需要联网获取dtd文件，mybatis的是XMLMapperEntityResolver
    private EntityResolver entityResolver;
    // 配置属性，在 properties 元素体内指定的属性首先被读取。
    private Properties variables;
    // XPath对象
    private XPath xpath;
}
```



#### 2.2、创建Document

```java
public class XPathParser {
    private Document createDocument(InputSource inputSource) {
        // important: this must only be called AFTER common constructor
        try {
            //这个是DOM解析方式
            DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
            factory.setValidating(validation);

            //名称空间
            factory.setNamespaceAware(false);
            //忽略注释
            factory.setIgnoringComments(true);
            //忽略空白
            factory.setIgnoringElementContentWhitespace(false);
            //把 CDATA 节点转换为 Text 节点
            factory.setCoalescing(false);
            //扩展实体引用
            factory.setExpandEntityReferences(true);

            DocumentBuilder builder = factory.newDocumentBuilder();
            //需要注意的就是定义了EntityResolver(XMLMapperEntityResolver)，这样不用联网去获取DTD，
            //将DTD放在org\apache\ibatis\builder\xml\mybatis-3-config.dtd,来达到验证xml合法性的目的
            builder.setEntityResolver(entityResolver);
            builder.setErrorHandler(new ErrorHandler() {
                ...
            });
            return builder.parse(inputSource);
        } catch (Exception e) {
            throw new BuilderException("Error creating document instance.  Cause: " + e, e);
        }
    }
}
```

以上就是Document创建代码，而另一个重要属性xpath则跟之前的demo一样创建

>XPathFactory factory = XPathFactory.newInstance();
>this.xpath = factory.newXPath();



#### 2.3、XPathParser获取元素

以String类型为例：

```java
public class XPathParser {

    /**
     * 根据xml路径获取元素值
     */
    public String evalString(String expression) {
        return evalString(document, expression);
    }
    
    /**
     * 获取元素的值
     * 
     * @param root Document 或者 Node
     * @param expression 表达式，可以是绝对路径如 /xml/user, 也可以是相对路径 user
     * @return 元素的值
     */
    public String evalString(Object root, String expression) {
        //1.先用xpath解析
        String result = (String) evaluate(expression, root, XPathConstants.STRING);
        //2.再调用PropertyParser去解析,也就是替换 ${}、#{} 这种格式的字符串
        result = PropertyParser.parse(result, variables);
        return result;
    }

    /**
     * 调用底层XPath的方法获取值
     */
    private Object evaluate(String expression, Object root, QName returnType) {
        try {
            //最终合流到这儿，直接调用XPath.evaluate
            return xpath.evaluate(expression, root, returnType);
        } catch (Exception e) {
            throw new BuilderException("Error evaluating XPath.  Cause: " + e, e);
        }
    }
}
```



### 3、PropertyParser

XPathParser获取元素值方法里面有使用PropertyParser进一步处理元素值，它是为了处理${}、#{} 这种格式的字符串，

把他们替换成XPathParser对象的属性variables，里面对应的属性值，如果找不到则维持原字符串

```java
/**
 * 属性解析器
 */
public class PropertyParser {
    
  public static String parse(String string, Properties variables) {
    VariableTokenHandler handler = new VariableTokenHandler(variables);
    GenericTokenParser parser = new GenericTokenParser("${", "}", handler);
    return parser.parse(string);
  }

  //就是一个map，用相应的value替换key
  private static class VariableTokenHandler implements TokenHandler {
    private Properties variables;

    public VariableTokenHandler(Properties variables) {
      this.variables = variables;
    }

    //待GenericTokenParser去除了开始和结束记号后，便会回调这个方法用相应的value替换key
    @Override
    public String handleToken(String content) {
      if (variables != null && variables.containsKey(content)) {
        return variables.getProperty(content);
      }
      return "${" + content + "}";
    }
  }
}
```

而GenericTokenParser则负责逐个去除开始和结束记号, 从左到右匹配， 并调用VariableTokenHandler#handleToken把属性值替换

```java
/**
 * 普通记号解析器，处理#{}和${}参数
 * 
 */
public class GenericTokenParser {

  //有一个开始和结束记号
  private final String openToken;
  private final String closeToken;
  //记号处理器
  private final TokenHandler handler;

  public GenericTokenParser(String openToken, String closeToken, TokenHandler handler) {
    this.openToken = openToken;
    this.closeToken = closeToken;
    this.handler = handler;
  }

  public String parse(String text) {
    StringBuilder builder = new StringBuilder();
    if (text != null && text.length() > 0) {
      char[] src = text.toCharArray();
      int offset = 0;
      int start = text.indexOf(openToken, offset);
      //#{favouriteSection,jdbcType=VARCHAR}
      //这里是循环解析参数，参考GenericTokenParserTest,比如可以解析${first_name} ${initial} ${last_name} reporting.这样的字符串,里面有3个 ${}
      while (start > -1) {
    	  //判断一下 ${ 前面是否是反斜杠，这个逻辑在老版的mybatis中（如3.1.0）是没有的
        if (start > 0 && src[start - 1] == '\\') {
          // the variable is escaped. remove the backslash.
      	  //新版已经没有调用substring了，改为调用如下的offset方式，提高了效率
          //issue #760
          builder.append(src, offset, start - offset - 1).append(openToken);
          offset = start + openToken.length();
        } else {
          int end = text.indexOf(closeToken, start);
          if (end == -1) {
            builder.append(src, offset, src.length - offset);
            offset = src.length;
          } else {
            builder.append(src, offset, start - offset);
            offset = start + openToken.length();
            //获得去除左右记号的字符串
            String content = new String(src, offset, end - offset);
            //得到一对大括号里的字符串后，调用handler.handleToken,比如替换变量这种功能
            builder.append(handler.handleToken(content));
            offset = end + closeToken.length();
          }
        }
        start = text.indexOf(openToken, offset);
      }
      if (offset < src.length) {
        builder.append(src, offset, src.length - offset);
      }
    }
    return builder.toString();
  }
}
```



### 4、XNode

XNode是对Node的封装，比较简单，我们来看一下它的属性

```java
public class XNode {

    //org.w3c.dom.Node
    private Node node;
    //以下都是预先把信息都解析好，放到map等数据结构中（内存中）
    private String name;
    private String body;
    private Properties attributes;
    private Properties variables;
    //XPathParser方便xpath解析
    private XPathParser xpathParser;

    /**
     * 构造方法
     * 
     * @param xpathParser xpathParser的this对象，获取元素值时用到(下传)
     * @param node org.w3c.dom.Node
     * @param variables xpathParser的variables属性(下传)
     */
    public XNode(XPathParser xpathParser, Node node, Properties variables) {
        this.xpathParser = xpathParser;
        this.node = node;
        this.name = node.getNodeName();
        this.variables = variables;
        // 调用Node#getAttributes()，放到Properties类型的attributes对象里面
        this.attributes = parseAttributes(node);
        // 解析自身元素值
        this.body = parseBody(node);
    }
}
```

在构造时就把一些信息（属性，body）全部解析好，以便我们直接通过getter函数取得

如:

```java
public class XNode {
    
    public String getStringBody() {
        return getStringBody(null);
    }
    
    public String getStringAttribute(String name) {
        return getStringAttribute(name, null);
    }
}
```



以及它也具有根据xml路径获取元素值的功能，该功能是通过传入的xpathParser对象来做到的

```java
public class XNode {
    
    /**
     * 根据xml路径获取元素值
     * 
     * @param expression 表达式，可以是绝对路径如 /xml/user, 也可以是相对路径 user
     * @return 元素值
     */
    public String evalString(String expression) {
        return xpathParser.evalString(node, expression);
    }
}
```
