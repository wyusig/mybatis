package org.apache.ibatis.parsing;

import org.junit.Test;
import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.xml.sax.InputSource;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathFactory;

import java.io.StringReader;

import static org.junit.Assert.assertEquals;

public class XPathTest {

    @Test
    public void test() throws Exception {
        String xml = "<xml><user><![CDATA[zxd]]></user></xml>";
        StringReader stringReader = new StringReader(xml);

        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        DocumentBuilder builder = factory.newDocumentBuilder();
        Document document = builder.parse(new InputSource(stringReader));

        XPathFactory xPathFactory = XPathFactory.newInstance();
        XPath xpath = xPathFactory.newXPath();

        String user1 = (String) xpath.evaluate("/xml/user", document, XPathConstants.STRING);
        assertEquals("zxd", user1);

        Node userNode = (Node) xpath.evaluate("xml", document, XPathConstants.NODE);
        String user2 = (String) xpath.evaluate("user", userNode, XPathConstants.STRING);
        assertEquals("zxd", user2);
    }
}
